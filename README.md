# HTML-CSS-Projects-Index

All The HTML/CSS Project Repositories

|  #  | Project                                                                                 | Live Demo                                                           |
| :-: | --------------------------------------------------------------------------------------- | ------------------------------------------------------------------- |
| 01  | [Training With Boxes](https://gitlab.com/mehmetozcelik2021/training-with-boxes)                              | [Live](https://mehmet-ozcelik.github.io/training-with-boxes/)               |
| 02  | [Google Landing Page](https://gitlab.com/mehmet-ozcelik/google-landing-page)                              | [Live](https://mehmet-ozcelik.github.io/google-landing-page/)               |
| 03  | [Flexbox Training](https://gitlab.com/mehmetozcelikk/flexbox-training)                              | [Live](https://mehmet-ozcelik.github.io/flexbox-training/)               |

